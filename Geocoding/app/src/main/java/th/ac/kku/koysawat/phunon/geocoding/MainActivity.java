package th.ac.kku.koysawat.phunon.geocoding;

import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView infoText;
    RadioGroup rg;
    EditText lat,lng,addr;
    Button btn;
    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = findViewById(R.id.btn);
        infoText = findViewById(R.id.infoText);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                String result = null;
                if(rg.getCheckedRadioButtonId() == R.id.ENlatlng){
                    try {
                        List<Address> list = geocoder.getFromLocation(Double.parseDouble(lat.getText().toString()), Double.parseDouble(lng.getText().toString()), 1);
                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = address.getAddressLine(0) + ", " + address.getLocality();
                            infoText.setText(result);
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Impossible to connect to Geocoder", e);
                    }
                } else {
                    try {
                        List<Address> list = geocoder.getFromLocationName(addr.getText().toString(), 1);
                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = "Lat: " + address.getLatitude() + "\n" + "Lng: " + address.getLongitude();
                            infoText.setText(result);
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Impossible to connect to Geocoder", e);
                    }
                }
            }
        });
        rg = findViewById(R.id.rg);
        lat = findViewById(R.id.lat);
        lng = findViewById(R.id.lng);
        addr = findViewById(R.id.addr);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (rg.getCheckedRadioButtonId()) {
                    case R.id.ENaddr:
                        addr.requestFocus();
                        lat.setEnabled(false);
                        lng.setEnabled(false);
                        addr.setEnabled(true);
                        break;
                    case R.id.ENlatlng:
                        lat.requestFocus();
                        lat.setEnabled(true);
                        lng.setEnabled(true);
                        addr.setEnabled(false);
                        break;
                }
            }
        });
    }
}
