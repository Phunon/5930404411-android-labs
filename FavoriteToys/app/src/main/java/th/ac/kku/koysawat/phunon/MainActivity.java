package th.ac.kku.koysawat.phunon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView green = (TextView) findViewById(R.id.green);
        green.setText("Green");
        green.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
        green.setGravity(Gravity.CENTER);
    }
}
