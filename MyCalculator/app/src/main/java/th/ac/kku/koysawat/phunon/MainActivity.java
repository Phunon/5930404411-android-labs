package th.ac.kku.koysawat.phunon;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText stNum;
    EditText ndNum;
    TextView textView,txtShow;
    Button btn;
    RadioGroup rg;
    Switch sw;
    float v1,v2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stNum = (EditText) findViewById(R.id.stNum);
        ndNum = (EditText) findViewById(R.id.ndNum);
        textView = (TextView) findViewById(R.id.result);
        txtShow = (TextView) findViewById(R.id.txtShow);
        rg = (RadioGroup) findViewById(R.id.rgOperator);
        btn = (Button) findViewById(R.id.button);
        sw = (Switch) findViewById(R.id.sw);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    txtShow.setText("ON");

                } else {
                    txtShow.setText("OFF");
                }
            }
        });
        btn.setOnClickListener(this);
        // Size Display
        Display display = getWindowManager().getDefaultDisplay();
        Point size =  new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        Toast.makeText(MainActivity.this, "Width = " + width + ", Height = " + height, Toast.LENGTH_SHORT).show();
    }

    public void onClick(View v) {
        if (v == btn) {
            calculate(1);
                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        calculate(i);
                    }
                });
        }
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void showToast(String msg) {
        Context context = getApplicationContext();
        CharSequence text = msg;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    private void calculate(int id) {
        float result = 0;
        long startTime = System.currentTimeMillis();
        acceptNumbers();
            switch (rg.getCheckedRadioButtonId()) {
                case R.id.plus:
                    result = v1 + v2;
                    break;
                case R.id.minus:
                    result = v1 - v2;
                    break;
                case R.id.times:
                    result = v1 * v2;
                    break;
                case R.id.divide:
                    if (v2 == 0) {
                        showToast("Please divide by a non-zero number");
                        break;
                    }
                    result = v1 / v2;
                    break;
            }
            textView.setText("= " + result);
            long endTime = System.currentTimeMillis();
            Log.i("Calculation", "Computation time = " + (endTime-startTime));
    }

    private void acceptNumbers() {
        try {
            v1 = Float.parseFloat("" + stNum.getText());
            v2 = Float.parseFloat("" + ndNum.getText());
        } catch (Exception e) {
            showToast("Please enter only a number");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString("result",textView.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        textView.setText(savedInstanceState.getString("result"));
    }
}
