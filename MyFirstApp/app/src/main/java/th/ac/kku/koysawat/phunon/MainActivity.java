package th.ac.kku.koysawat.phunon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                EditText editName = (EditText) findViewById(R.id.editName);
                EditText editPhone = (EditText) findViewById(R.id.editPhone);
                String strName = editName.getText().toString();
                String strPhone = editPhone.getText().toString();
                intent.putExtra("name", strName);
                intent.putExtra("phone", strPhone);
                startActivity(intent);
            }
        });
    }
}
