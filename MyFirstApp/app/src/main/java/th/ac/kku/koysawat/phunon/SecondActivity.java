package th.ac.kku.koysawat.phunon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();
        TextView textView = (TextView) findViewById(R.id.txtView);
        textView.setText(intent.getStringExtra("name") + getResources().getString(R.string.show) + intent.getStringExtra("phone"));
    }
}
