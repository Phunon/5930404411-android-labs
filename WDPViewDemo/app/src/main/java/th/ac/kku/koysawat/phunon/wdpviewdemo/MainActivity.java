package th.ac.kku.koysawat.phunon.wdpviewdemo;

import android.content.Context;
import android.graphics.Point;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText stNum;
    EditText ndNum;
    TextView textView;
    Button btn;
    float v1, v2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stNum = (EditText) findViewById(R.id.stNum);
        ndNum = (EditText) findViewById(R.id.ndNum);
        textView = (TextView) findViewById(R.id.result);
        btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(this);
        // Size Display
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        Toast.makeText(MainActivity.this, "Width = " + width + ", Height = " + height, Toast.LENGTH_SHORT).show();
    }

    public void onClick(View v) {
        if (v == btn) {
            acceptNumbers();
            float result = v1 + v2;
            textView.setText("= " + result);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void showToast(String msg) {
        Context context = getApplicationContext();
        CharSequence text = msg;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    private void acceptNumbers() {
        try {
            v1 = Float.parseFloat("" + stNum.getText());
            v2 = Float.parseFloat("" + ndNum.getText());
        } catch (Exception e) {
            showToast("Please enter only a number");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("v1", stNum.getText().toString());
        outState.putString("v2", ndNum.getText().toString());
        outState.putString("result", textView.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        stNum.setText(savedInstanceState.getString("v1"));
        ndNum.setText(savedInstanceState.getString("v2"));
        textView.setText(savedInstanceState.getString("result"));
    }
}